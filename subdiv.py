# -*- coding: utf-8 -*-
import cv2
import numpy as np
import random
from math import isclose
from tkinter import filedialog, Tk

root = Tk()
root.withdraw()
root.filename =  filedialog.askopenfilename(title = "Select file",filetypes = (("all files","*.*"),("jpeg files","*.jpg")))
print(root.filename)

img = cv2.imread(root.filename)
root.destroy()
gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

for i in range(gray.shape[0]):
    gray[0][i] = 255
    gray[i][0] = 255
    gray[i][gray.shape[0]-1] = 255
    gray[gray.shape[0]-1][i] = 255
    
cv2.imshow('gray', gray)
gray = cv2.medianBlur(gray,9)

cv2.imshow('filtered', gray)
edges = cv2.Canny(gray,100,200,apertureSize = 3)

cv2.imshow('edges', edges)

im2, contours, hierarchy = cv2.findContours(edges ,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

#for p,i in enumerate(contours):
#    cv2.drawContours(img, i, -1, ( random.randint(0,256), random.randint(0,256), random.randint(0,256)) , 4)
#    cv2.imshow('img', img)
#    cv2.waitKey()

contours.sort(key=cv2.contourArea, reverse = True)
contours = contours[::2]

color_of_contours = []
for i in contours:
    epsilon = 0.1*cv2.arcLength(i,True)
    i = cv2.approxPolyDP(i,epsilon,True)
    
    mask = np.zeros(gray.shape,np.uint8)
    cv2.drawContours(mask,i,0,255,-1)
    color_of_contours.append(cv2.mean(gray,mask=mask))

for i in contours:  
    cv2.fillPoly(img, pts =[i], color=( random.randint(0,256), random.randint(0,256), random.randint(0,256)))

size = img.shape
rect = (0, 0, size[1], size[0])

cv2.imshow('img', img)

f = open('path.svg', 'w+')
f.write('<svg width="'+str(img.shape[0])+'" height="'+str(img.shape[1]) + '">\n')
f.write('<path d="M ')
for numb,j in enumerate(contours):

    for number, i in enumerate(j):
        print('i= ', i)
        x, y = i[0]
        print(x)
        if number ==0:
            f.write( str(x)+  ' ' + str(y)+' ')
        else:
            f.write( str(x)+  ' ' + str(y)+' ')
    f.write( ' Z M ')

f.write('" fill="black" stroke="black" stroke-width="1" fill-rule="evenodd"/>\n')
f.write('</svg>')
f.close()

cv2.waitKey()

cv2.destroyAllWindows()



