import cv2
import numpy as np
from math import isclose
import triangulation

img = cv2.imread('files//like-1.png')

gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

for i in range(gray.shape[0]):
    gray[0][i] = 255
    gray[i][0] = 255
    gray[i][gray.shape[0]-1] = 255
    gray[gray.shape[0]-1][i] = 255

cv2.imshow('gray',gray)  

gray = cv2.bilateralFilter(gray,9,75,75)
edges = cv2.Canny(gray,80,200,apertureSize = 3)
cv2.imshow('edges',edges)

im2, contours, hierarchy = cv2.findContours(edges ,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
#cv2.drawContours(img, contours, -1, (0,255,0), 1)
contours.sort(key=cv2.contourArea, reverse=True) 


#size = gray.shape
#rect = (0, 0, size[1], size[0])
#subdiv = cv2.Subdiv2D(rect)
#for i in contours[0]:
#    for p in i:
#        subdiv.insert(p)

#triangulation.draw_delaunay(img, subdiv, (255,255,0))
#lines = cv2.HoughLines(edges,1,np.pi/180, 1)
#for rho,theta in lines[0]:
#    a = np.cos(theta)
#    b = np.sin(theta)
#    x0 = a*rho
#    y0 = b*rho
#    x1 = int(x0 + 1000*(-b))
#    y1 = int(y0 + 1000*(a))
#    x2 = int(x0 - 1000*(-b))
#    y2 = int(y0 - 1000*(a))
#
#    cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)


#    minLineLength = 100
#    maxLineGap = 1
#    lines = cv2.HoughLinesP(edges, 1, 0.0001,200,minLineLength,maxLineGap)
#    for x1,y1,x2,y2 in lines[0]:
#        print("Gotcha!")
#        cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)

cv2.imshow('img',img)

# от большого к маленькому

color_of_contours = []
for i in contours:
    mask = np.zeros(gray.shape,np.uint8)
    cv2.drawContours(mask,i,0,255,-1)
    color_of_contours.append(cv2.mean(gray,mask=mask))
#c = contours
c = contours


#    c = max(contours, key=cv2.contourArea) #max contour


f = open('path.svg', 'w+')
f.write('<svg width="'+str(img.shape[0])+'" height="'+str(img.shape[1]) + '">\n')

kkk = 0

for numb,j in enumerate(c):
    #print(c[i][0])'
#        f.write('<path d="M ')
    kkk+=1
    f.write('<path d="M ')
    
    
    for number, i in enumerate(j):
        print('i= ', i)
        x, y = i[0]
        print(x)
        f.write(str(x)+  ' ' + str(y)+' ')
        
#    f.write(' " />\n')
#    f.write(' " fill="transparent" stroke="black"/>\n')
    if isclose( color_of_contours[numb][0], 255, abs_tol=15):
        f.write('" fill="white" />\n')
    else:
        f.write('" fill="black" />\n')


        
        
    
#    
 


f.write('</svg>')
f.close()
cv2.waitKey()
cv2.destroyAllWindows()
print(kkk)

