import cv2
import numpy as np

img = cv2.imread('files//bluetooth.png')

gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)


edges = cv2.Canny(gray,20,200,apertureSize = 3)
cv2.imshow('edges',edges)

lines = cv2.HoughLinesP(edges, 1, np.pi / 180 ,  0 , 0,0 ,0)

#for x1,y1,x2,y2 in lines[0]:
#    print("Gotcha!")
#    cv2.line(gray,(x1,y1),(x2,y2),(0,255,0),4)



f = open('path_fl.svg', 'w+')
f.write('<svg width="'+str(img.shape[0])+'" height="'+str(img.shape[1]) + '">\n')

line_count = 0
for i in lines:
    
    for x1,y1,x2,y2 in i:
        f.write('<path d="M ' + str(x1) + ',' + str(y1) + ' L ' + str(x2) + ',' + str(y2) +'" style="stroke:rgb(255,0,0);stroke-width:2" />\n')
        cv2.line(img,(x1,y1),(x2,y2),(0,255,0),4)
        line_count +=1

f.write('</svg>')

cv2.imshow('img',img)
print(line_count)

f.close()
cv2.waitKey()
cv2.destroyAllWindows()