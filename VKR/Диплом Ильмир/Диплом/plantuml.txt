class Bundler {
 String mainFilename
 BundleOptions options
 Parser parser
 PackagerRegistry packagerRegistry
 Resolver resolver
 Asset mainAsset
 Asset bundle()
 Promise<Bundle> createBundleTree(asset: Asset, dep?: Dependency, bundle?: Bundle)
 Promise<Asset> resolveAsset(filename: string, parent?: string)
 Promise<any> loadAsset(asset: Asset)
 Object processFile(filename: string)
}

class Resolver {
 ResolverOptions options
 Map cache
 String resolve(filename, parent)
 String resolveInternal(filename, parent)
 String getCacheKey(filename, parent)
 String saveCache(filename, parent, resolved)
}

class Parser{
 Object extensions
 Object getExtensions()
 Asset getAsset(filename)
 void registerExtensions(extnames, assetParser)
 Asset findAsset(filename): new(filename: string, options: AssetOptions)
}

class Packager {
 Bundle bundle
 Bundler bundler
 Object options
 String dest
 void setup()
 void start()
 void addAsset(asset: Asset)
 void end()
}

class Asset {
 Number id
 Boolean processed
 Map<String, Dependency> dependencies
 Map<String, Asset> depAssets
 String filename
 Object generated
 String ast
 String content
 FileType type
 Boolean isES6Module
 Boolean isAstDirty
 Bundle parentBundle
 Set<Bundle> bundles
 AssetOptions options
 void addDependency(name, opts)
 String addURLDependency(url)
 void async loadIfNeeded()
 void async parseIfNeeded()
 void async getDependencies()
 Promise async loadFile()
 Promise async parse(code: String)
 Promise async collectDependencies()
 Promise async transform()
 void generate()
 void async process()
 String generateBundleName()
}

class Bundle {
 Asset entryAsset
 Set<Asset> assets
 String name
 FileType type 
 Object options
 Bundle parentBundle
 Set<Bundle> childBundles
 Map<FileType, Bundle> siblingBundles
 void addAsset(asset: Asset)
 Bundle getSiblingBundle(type: FileType)
  Bundle createChildBundle(type: FileType, name: string)
 void async package(bundler: Bundler)
 void async internalPackage(bundler: Bundler)
 void async addDeps(asset: Asset, packager: Packager, included: Set<Asset>)
 String getSiblingName(type: FileType)
}