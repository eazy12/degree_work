from os import listdir
import cv2
from svgpathtools import svg2paths

from sklearn import svm
from xml.dom import minidom

path_to_png = r'/home/eazy12/Desktop/work/degree_work/ml/png'
path_to_svg = r'/home/eazy12/Desktop/work/degree_work/ml/svg'

images = []
svg_images = []
images_with_paths_only = []

for i in listdir(path_to_svg):
#    paths, attributes = svg2paths(path_to_svg + '//' + i)
#    svg_images.append( (i,  paths, attributes))
    doc = minidom.parse( path_to_svg + '//' + i)  
    path_strings = [path.getAttribute('d') for path
                in doc.getElementsByTagName('path')]
    polygon_strings = [path.getAttribute('points') for path
                in doc.getElementsByTagName('polygon')]
    rect_strings = [(path.getAttribute('y'), path.getAttribute('width'), \
                    path.getAttribute('height')) for path \
                in doc.getElementsByTagName('rect')]
    if polygon_strings == [] and rect_strings == []:
        images_with_paths_only.append(i)
#    svg_images.append( (path_strings, polygon_strings, rect_strings ))
    doc.unlink()

for i in images_with_paths_only:
    _temp = cv2.imread(path_to_png + '//' + i[:-3] + 'png')
    images.append( cv2.cvtColor(_temp, cv2.COLOR_RGB2GRAY).flatten()  )
    
    paths, attributes = svg2paths(path_to_svg + '//' + i)
    svg_images.append(attributes)
    
pass
#images.sort()
#svg_images.sort()
clf = svm.SVC(gamma=0.001, C=100.)


clf.fit(images[:], svg_images) 




#from svgpathtools import svg2paths
#paths, attributes = svg2paths(i)









#
#
#
#gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
#cv2.imshow('gray',gray)
#cv2.waitKey()
#
#edges = cv2.Canny(gray,50,150,apertureSize = 3)
#cv2.imshow('edges',edges)
#cv2.waitKey()
#im2, contours, hierarchy = cv2.findContours(edges ,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
#cv2.drawContours(img, contours, -1, (0,255,0), 3)
#
#
#
##lines = cv2.HoughLines(edges,1,np.pi/180, 1)
##for rho,theta in lines[0]:
##    a = np.cos(theta)
##    b = np.sin(theta)
##    x0 = a*rho
##    y0 = b*rho
##    x1 = int(x0 + 1000*(-b))
##    y1 = int(y0 + 1000*(a))
##    x2 = int(x0 - 1000*(-b))
##    y2 = int(y0 - 1000*(a))
##
##    cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
#
#
#minLineLength = 100
#maxLineGap = 1
#lines = cv2.HoughLinesP(edges, 1, 0.0001,200,minLineLength,maxLineGap)
#for x1,y1,x2,y2 in lines[0]:
#    print("Gotcha!")
#    cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)
#
#cv2.imshow('img',img)
#cv2.waitKey()
#
##c = contours
#c = max(contours, key=cv2.contourArea) #max contour
#f = open('path.svg', 'w+')
#f.write('<svg width="'+str(800)+'" height="'+str(799)+'" xmlns="http://www.w3.org/2000/svg">')
#f.write('<path d="M ')
#
#for i in range(0,len(c)):
#    #print(c[i][0])'
#    
#    print('i= ', i)
#    x, y = c[i][0]
#    print(x)
#    f.write(str(x)+  ' ' + str(y)+' ')
#
#f.write('"/>\n')
#
#
#f.write('</svg>')
#f.close()
#cv2.destroyAllWindows()
#
