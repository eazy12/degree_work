from os import listdir
#from svgpathtools import svg2paths
import re

def mult(ss):
    global k
    ss = ss.group(0)
    if ss.isdigit():
        return str(float(ss) * k)
    else:
        return str(float(ss) * k)

for i in listdir():
    if i[-3:] == '.py':
        continue

    f = open(i, 'r+')
    text = f.read()

    src = text

    index = text.find('viewBox="0 0 ')
    index2 = index + 11
    index3 = text.find('"', index2)
    kappa = text[index : index3+1]
    kappa = kappa[13:-1].split()
    size = float(kappa[0])
    k = 512 / size
    f.close()
    
    index4 = text.find('<polygon')
    index_tag_g = text.find('<g')
    index_tag_path = text.find('<path')
    
    min_index = 10000
    for hh in (index4, index_tag_g, index_tag_path):
        if hh == -1:
            continue
        else:
            if min_index > hh:
                min_index = hh
            
    
    index4 = min_index
    g_part = text[index4:]
    
    g_part = re.sub("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", mult, g_part)
    
    text1 = text[:index4] + g_part
    text1 = text1[:index+13] + '512 512' + text1[index3:]
    

    open(i, 'w').close()
    
    
    f = open(i, 'r+')
    f.write(text1)
    f.close()
    print("job done")
#    
#    width="800" height="799"
#    k = 512 / kappa[0] 
#    
#    paths, attributes = svg2paths(i)
##    
##    for j in attributes:
#        print(j)
#        
#        kkk = j['d'].split(',')
#        for z in kkk:
#            if  ord('a') <= ord(z[0]) <= ord('z') or \
#            ord('A') <= ord(z[0]) <= ord('Z'):
#                temp = z[1:]
#                temp = float(temp)
#                temp = temp * k
#                temp = z[0] +  str(temp)
#            elif is_number(z):
#                temp = float(z) * k
#            elif !is_number(z):
#                
